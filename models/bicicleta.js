var Bicicleta = function (id, color, modelo, ubicacion) {
	this.id = id;
	this.color = color;
	this.modelo = modelo;
	this.ubicacion = ubicacion;
};
// Redefino la funcion toString
Bicicleta.prototype.toString = function () {
	return `id: ${this.id} color: ${this.color}`;
};

// Agrego funciones
Bicicleta.allBicis = [];
Bicicleta.add = function (aBici) {
	Bicicleta.allBicis.push(aBici);
};
Bicicleta.findById = function (idBici) {
	var bici = Bicicleta.allBicis.find((x) => x.id == idBici);
	if (bici) {
		return bici;
	} else {
		throw new Error(`No se encontro la bicicleta ${idBici} en la base de datos`);
	}
};

Bicicleta.removeById = function (idBici) {
	var bici = Bicicleta.findById(idBici);
	Bicicleta.allBicis.forEach((elem, index, array) => {
		if (bici == elem) {
			array.splice(index, 1);
		}
	});
};

Bicicleta.update = function (id, color, modelo, latitud, longitud) {
	Bicicleta.allBicis.forEach((elem, index, array) => {
		if (elem.id == id) {
			array[index] = new Bicicleta(id, color, modelo, [latitud, longitud]);
		}
	});
};

// Seteo dos biciletas por default
var a = new Bicicleta(1, "rojo", "urbana",[-38.8807,-62.0662]) ;
var b = new Bicicleta(2, "blanco", "urbana",[-38.8857,-62.0670]);

Bicicleta.add(a);
Bicicleta.add(b);

module.exports = Bicicleta;
