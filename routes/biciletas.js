var express = require("express");
var router = express.Router();
var bicicletaController = require("../controllers/bicicletaController");


router.get("/", bicicletaController.bicicleta_list);
router.get("/show/:id",bicicletaController.bicicleta)
router.get("/crear",bicicletaController.bicicleta_create_get)
router.post("/crear",bicicletaController.bicicleta_create_post)
router.post("/:id/destroy",bicicletaController.bicicleta_delete_post)
router.get("/:id/update",bicicletaController.bicicleta_update_get)
router.post("/:id/update",bicicletaController.bicicleta_update_post)

module.exports = router;