

var map = L.map('main_map').setView([-38.8807, -62.0611],13 );

L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
	attribution:'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>',
}).addTo(map);

// https://openstreetmap.org.ar/#13.61/-38.87684/-62.07149
// L.marker([-38.8807,-62.0611]).addTo(map)
// L.marker([-38.8807,-62.0610]).addTo(map)
// L.marker([-38.8807,-62.0612]).addTo(map)

$.ajax({
	dataType :"json",
	url:"api/bicicletas",
	success: function(result){
		console.log(result)
		var {bicicletas}=result
		bicicletas.forEach(bicicleta => {
			let{ ubicacion} = bicicleta//posicion de la bicicleta
			L.marker(ubicacion,{title:bicicleta.id}).addTo(map)
			
		});
	}
})