
const Bicicleta = require("../models/bicicleta");

exports.bicicleta = function(req,res){
    var bici = Bicicleta.findById(req.params.id)
    res.render('bicicleta/bicicletaDetalle',{bici})
}
exports.bicicleta_list = function(req,res){
    res.render('bicicleta/index',{bicis:Bicicleta.allBicis})
}

exports.bicicleta_create_get = function(req,res){
    res.render('bicicleta/crear')
}

exports.bicicleta_create_post = function(req,res){
   var {id,modelo,color,latitud,longitud}= req.body
   var newBici = new Bicicleta(id,color,modelo,[latitud,longitud])
   Bicicleta.add(newBici)

   res.redirect('/bicicletas')

}

exports.bicicleta_delete_post = function(req,res){
    var  biciDestroyId = req.body.id
    Bicicleta.removeById(biciDestroyId)
    res.redirect('/bicicletas')
}

exports.bicicleta_update_get = function (req,res) {
    
    var bici = Bicicleta.findById(req.params.id)
    res.render("bicicleta/update",{bici})
}

exports.bicicleta_update_post = function (req,res) {
    var {id,color,modelo,latitud,longitud}= req.body
    Bicicleta.update(id,color,modelo,latitud,longitud)
    res.redirect("/bicicletas")
}
