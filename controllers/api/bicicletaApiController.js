var Bicicleta = require("../../models/bicicleta");

exports.bicicleta_list = function (req, res) {
	res.status(200).json({
		bicicletas: Bicicleta.allBicis,
	});
};

exports.bicicleta_create = function (req, res) {
	var { id, color, modelo, ubicacion } = req.body;
	var newBici = new Bicicleta(id, color, modelo, ubicacion);
	Bicicleta.add(newBici);
	res.status(200).json({
		bicicleta: newBici,
	});
};

exports.bicicleta_delete = function (req, res) {
	var { id } = req.body;
	Bicicleta.removeById(id);
	res.status(204).send();
};

exports.bicicleta_update = function (req, res) {
	var { id, color, modelo, ubicacion } = req.body;
	Bicicleta.update(id, color, modelo, ubicacion[0],ubicacion[1]);
	res.status(200).send();
};
